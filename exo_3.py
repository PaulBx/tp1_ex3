#!/usr/bin/env python
# coding : utf-8
"""
author : paul bouyssoux

-Ce script permet de créer une classe contenant les fonctions
 pour créer un calculateur simple (+,-,*,/).

"""


#Definition de la classe
class SimpleCalculator:
    """simple classe model, SimpleCalculator class"""

    def __init__(self, mon_entier_1, mon_entier_2):
        """constructeur : on associe deux variables correspondant
        aux deux entiers qui initialisent l'objet de la classe"""
        self.mon_entier_1 = mon_entier_1
        self.mon_entier_2 = mon_entier_2

    #Définition de la méthode somme
    def fsum(self):
        """On récupère les deux attributs de l'objet et on
        effectue la somme"""
        return self.mon_entier_1 + self.mon_entier_2

    #Définition de la méthode soustraction
    def substract(self):
        """On récupère les deux attributs de l'objet et on
        effectue la différence"""
        return self.mon_entier_1 - self.mon_entier_2

    #Définition de la méthode multiplier
    def multiply(self):
        """On récupère les deux attributs de l'objet et on
        effectue le produit"""
        return self.mon_entier_1 * self.mon_entier_2

    #D&finition de la méthode diviser
    def divide(self):
        """On récupère les deux attributs de l'objet et on
        effectue la division"""
        if self.mon_entier_2 != 0:
            return self.mon_entier_1 / self.mon_entier_2
        return "vous avez essayé de diviser par zéro"


#Lancement d'un programme executant le script lorsqu'on l'appelle dans un terminal
if __name__ == '__main__':
    MON_TEST = SimpleCalculator(10, 5) # on créé un objet de la classe
    print(MON_TEST.fsum())
    print(MON_TEST.substract())
    print(MON_TEST.multiply())
    print(MON_TEST.divide())


    MON_TEST2 = SimpleCalculator(10, 0) # on créé un autre objet de la classe
    print(MON_TEST2.fsum())
    print(MON_TEST2.substract())
    print(MON_TEST2.multiply())
    print(MON_TEST2.divide())
