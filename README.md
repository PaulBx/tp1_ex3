# Exercice 3 TP1

## Objectif
L'objectif de cet exercice est de réécrire le script de l'exercice en tenant compte des contraintes de bonnes pratiques PEP8.

## Outils pour la réalisation 
Afin de réécrire le plus proprement possible le script, nous avons à disposition principalement deux outils : Pylint et Black.

## Installation des outils
Pour installer les outils, il suffit d'entrer les commandes :  
    `pip install pylint`  
    `pip install black`  


## Réalisation
Afin de corriger notre produit, dans un premier j'ai consulté les ressources internet sur les bonnes pratiques afin de corriger dans un premier temps les erreurs évidentes (tabulations, nombre de lignes entre les méthodes etc..)
J'ai ensuite analysé mon script avec Pylint afin de le noter et de voir les erreurs restantes. J'ai corrigé les erreurs les unes après les autres jusqu'à avoir une note de 10/10 avec l'outil pylint :

    tp@tp-VM:~/Documents/tp1_ex3$ pylint exo_3.py 

    --------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)


## Lancement du programme
Le script reste toujours fonctionnel, pour l'éxécuter il faut dans un terminal se placer dans le dossier tp1_exo3, puis effectuer la commande :  

    tp@tp-VM:~/Documents/tp1_ex3$ python exo_3.py  

Pour observer la note pylint de notre script, au même emplacement entrer la commande :  

    tp@tp-VM:~/Documents/tp1_ex3$ pylint exo_3.py  



## Ressources
*PEP8 :*  
(https://openclassrooms.com/fr/courses/4425111-perfectionnez-vous-en-python/4464230-assimilez-les-bonnes-pratiques-de-la-pep-8)  
(https://python.doctor/page-pep-8-bonnes-pratiques-coder-python-apprendre)  
(https://python.sdv.univ-paris-diderot.fr/15_bonnes_pratiques/)  
(https://about.gitlab.com/handbook/business-ops/data-team/python-style-guide/)  
(https://blog.impulsebyingeniance.io/outils-et-bonnes-pratiques-pour-un-code-python-de-bonne-qualite/)  
(https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)  

*Black :*  
(https://python.doctor/page-black-code-formatter)  
 
*Pylint :*  
(https://realpython.com/python-code-quality/)  
